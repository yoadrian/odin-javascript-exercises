// even simpler code
// the expression is boolean, so no need to tell it what to return, it does automatically: https://stackoverflow.com/a/61245416

const leapYears = function(year) {
    return (((year % 100 !== 0) && (year % 4 === 0)) || ((year % 100 === 0) && (year % 400 === 0)));
}

leapYears(700);

/*
// simpler code
const leapYears = function(year) {
    return (((year % 100 !== 0) && (year % 4 === 0)) || ((year % 100 === 0) && (year % 400 === 0))) ? true : false;
}
*/

/*
const leapYears = function(year) {
    if (((year % 100 !== 0) && (year % 4 === 0)) || ((year % 100 === 0) && (year % 400 === 0))) {
        return true;
    } else {
        return false;
    }
}
*/


/*
Pseudocode:
if year is NOT divisible by 100 BUT IS divisible by 4 => leap year
if year IS divisible by 100 AND IS divisible by 400 => leap year
*/

// Do not edit below this line
module.exports = leapYears;