const reverseString = function(str) {
    let splittedString = str.split("");
    let reversedString = splittedString.reverse();
    let joinedString = reversedString.join("");
    return joinedString;
};

reverseString("");

/*
split string: split()
reverse string: reverse()
join new string: join()
*/

// Do not edit below this line
module.exports = reverseString;
