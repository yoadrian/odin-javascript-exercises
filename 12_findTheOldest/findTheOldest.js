const findTheOldest = function(array) {
    const orderedList = array.sort(function(person1, person2) {
      
      if (!person2.yearOfDeath) {
        person2.yearOfDeath = new Date().getFullYear();
      };
      
      if (!person1.yearOfDeath) {
       person1.yearOfDeath = new Date().getFullYear();
      };
      
      if ((person1.yearOfDeath - person1.yearOfBirth) < (person2.yearOfDeath - person2.yearOfBirth)) {
        return 1
      } else {
        return -1
      };
      
    });
      
      const oldestPerson = orderedList[0];
      return oldestPerson;
    };

// Do not edit below this line
module.exports = findTheOldest;
