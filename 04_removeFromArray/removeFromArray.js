const removeFromArray = function(...args) {
    const mainArgs = args[0];
    const result = [];

    for (const i of mainArgs) {
        if (!args.includes(i)) {
            result.push(i);
        }

        //return result;
    }
    return result;
}

/*const removeFromArray = function(...args) {
    const mainArgs = args[0];
    const result = [];

    mainArgs.forEach((item) => {
        if (!args.includes(item)) {
            result.push(item);
        }
    })
    return result;
}
*/

removeFromArray([1, 2, 3], "1", 3);

// Do not edit below this line
module.exports = removeFromArray;
