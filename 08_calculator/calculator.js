const add = function(a, b) {
	return a + b;
};
add(0, 0);
add(2, 2);
add(2, 6);

const subtract = function(a, b) {
	return a - b;
};
subtract(10, 4);

const sum = function(a) {
	let total = 0;
  for (let i = 0; i < a.length; i++) {
    total += a[i];
  }
  return total;
};
sum([]);
sum([7]);
sum([7, 11]);
sum([1,3,5,7,9]);

const multiply = function(a) {
  let result = 1;
  a.forEach(item => result *= item);
  return result;
};
multiply([2,4]);
multiply([2,4,6,8,10,12,14]);

const power = function(a, b) {
	let result = 1;
  for (let i = 0; i < b; i++) {
    result = result * a;
  }
  return result;
};
power(4,3);

const factorial = function(a) {
	let result = a;
  
  if (a === 0) {
    result = 1;
  } else {
    for (let i = 1; i < a; i++) {
      result = result * (a - i);
    }
  }
  return result;
};
factorial(0);
factorial(1);
factorial(2);
factorial(5);
factorial(10);


// Do not edit below this line
module.exports = {
  add,
  subtract,
  sum,
  multiply,
  power,
  factorial
};
