// for the second to last test, I just had to write this line underneath
// const number = Math.floor(Math.random() * 1000);

const repeatString = function(string, num) {
    if (num >= 0) {
        let rstring = string.repeat(num);
        return rstring;
    } else {
        return 'ERROR';
    }
};

repeatString('', 10);

// Do not edit below this line
module.exports = repeatString;

/* 
iterate 4 times with a loop
    generate random number
    if number is positive
    multiply string 'hey' as many times as the random number
    if number is negative, 
*/