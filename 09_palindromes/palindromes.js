const palindromes = function(string) {
    //process original string into: no puctuation, no space, lower case
    let punctuation = '!"#$%&\'()*+,-./:;<=>?@[\\]^_`{|}~';    //string containing punctuation marks
    const processedOriginal = string
                                .split("")                     //splits string into array of characters
                                .filter(function(letter) {
    return punctuation.indexOf(letter) === -1;
  })                                                           //filters out the punctuation marks from the array
                                .join('')                      //joins the string back together
                                .split(' ')                    //splits into an array of words divided by spaces
                                .join('')                      //joins into string with no spaces
                                .toLowerCase();                //converts string to lower case
    
    //reverse above result
    const reversedString = processedOriginal
      .split('')                                              //splits the string into an array of characters
      .reverse()                                              //reverses the order of array elements
      .join('');                                              //joins the reversed array back together into a string
    if (processedOriginal === reversedString) {
      return true
    } else {
      return false
    };
  };

  palindromes('racecar');
  palindromes('racecar!');
  palindromes('Racecar!');
  palindromes('A car, a man, a maraca.');
  palindromes('Animal loots foliated detail of stool lamina.');
  palindromes('ZZZZ car, a man, a maracaz.');

// Do not edit below this line
module.exports = palindromes;
