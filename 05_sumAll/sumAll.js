const sumAll = function(a, b) {
let sum = 0;

if ((a < 0) || (b < 0) || (typeof a !== 'number') || (typeof b !== 'number')) {
    return 'ERROR';
} else if (a <= b) {
    for (i = a; i <= b; i++) {
        sum += i;
    }
    return sum;
} else {
    for (i = b; i <= a; i++) {
        sum += i;
    }
    return sum;
}
};

sumAll(10, [90, 1]);

/*
given parameters a and b
let sum = 0
if a <= b
for i = a, i <= b, i++
sum += i

else 
for i = b, i<= a, i++
sum += i

return sum
*/

// check alternate TOP solution here https://github.com/TheOdinProject/javascript-exercises/blob/solutions/sumAll/sumAll.js

// Do not edit below this line
module.exports = sumAll;
