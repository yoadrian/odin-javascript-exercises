const ftoc = function(fTemp) {
  return result = Math.round(((fTemp - 32) * 5 / 9) * 10) /10;
};

ftoc(-100);

const ctof = function(cTemp) {
  return result = Math.round(((cTemp * 9 / 5) + 32) * 10) /10;
};

ctof(-10);

// Do not edit below this line
module.exports = {
  ftoc,
  ctof
};
