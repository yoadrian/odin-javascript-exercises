const fibonacci = function(number) {
    result = [0, 1];
    if (number < 0) {
      return 'OOPS';
    } else if (number == 0) {
      return 0;
    } else if (number == 1) {
      return 1;
    } else {
      for (let i = 2; i <= number; i++) {
      result = result.concat(result.splice(i, 0, result[i-1] + result[i-2]));
      fibonacciNumber = result[i];
      }
      return fibonacciNumber;
    }
  };
  fibonacci(4);
  fibonacci(6);
  fibonacci(10);
  fibonacci(15);
  fibonacci(25);
  fibonacci(-25);
  fibonacci("1");
  fibonacci("2");
  fibonacci("8");

// Do not edit below this line
module.exports = fibonacci;
